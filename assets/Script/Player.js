cc.Class({
  extends: cc.Component,

  properties: {
    jumpHeight: 0,
    jumpDuration: 0,
    maxMoveSpeed: 0,
    accel: 0,
    jumpAudio: {
      default: null,
      type: cc.AudioClip,
    },
    squashDuration:0
  },

  // LIFE-CYCLE CALLBACKS:

  onLoad () {
    let actions = this.setJumpAction()
    this.node.runAction(actions)

    //set move left or right
    this.accLeft = false
    this.accRight = false
    //set xSpeed
    this.xSpeed = 0

    this.minPosX = - this.node.parent.width / 2;
    this.maxPosX = -this.minPosX;

    //init setInputControl
    this.setInputControl()
  },

  playJumpAudio: function () {
    cc.audioEngine.playEffect(this.jumpAudio, false)
  },

  setJumpAction: function () {
    let easeObj = cc.easeCubicActionOut()
    let deltaPos = cc.p(0, this.jumpHeight)
    let jumpUp = cc.moveBy(this.jumpDuration, deltaPos).easing(easeObj)

    easeObj = cc.easeCubicActionIn()
    deltaPos = cc.p(0, -this.jumpHeight)
    let jumpDown = cc.moveBy(this.jumpDuration, deltaPos).easing(easeObj)
    let callBack = cc.callFunc(this.playJumpAudio, this)
    let squash = cc.scaleTo(this.squashDuration,1, 0.6)
    let sketch = cc.scaleTo(this.squashDuration,1, 1.2)
    let scaleBack = cc.scaleTo(this.squashDuration,1, 1)
    let actions = cc.sequence(squash, sketch, jumpUp, scaleBack, jumpDown, callBack)

    return cc.repeatForever(actions)
  },

  setInputControl: function () {
    var self = this
    cc.eventManager.addListener({
      event: cc.EventListener.KEYBOARD,
      onKeyPressed: function (keyCode, event) {
        console.log({keyCode})
        switch (keyCode) {
          case cc.KEY.a:
            self.accLeft = true
            self.accRight = false
            break
          case cc.KEY.d:
            self.accLeft = false
            self.accRight = true
            break
        }
      },
      onKeyReleased: function (keyCode, event) {
        switch (keyCode) {
          case cc.KEY.a:
            self.accLeft = false
            break
          case cc.KEY.d:
            self.accRight = false
            break
        }
      },
    }, self.node)

    this.node.parent.on(cc.Node.EventType.TOUCH_START, function (event) {
      let touchs = event.getTouches();
      let touchLoc = touchs[0].getLocation();
      if(touchLoc.x >= cc.winSize.width / 2) {
        self.accRight = true
        self.accLeft = false
      }else {
        self.accRight = false
        self.accLeft = true
      }
    }, this)
    this.node.parent.on(cc.Node.EventType.TOUCH_END, function (event) {
        self.accRight = false
        self.accLeft = false
    }, this)
  },

  update (dt) {

    if (this.accLeft) {
      this.xSpeed -= this.accel * dt
    } else if (this.accRight) {
      this.xSpeed += this.accel * dt
    }

    if (Math.abs(this.xSpeed) > this.maxMoveSpeed) {
      this.xSpeed = this.maxMoveSpeed * this.xSpeed / Math.abs(this.xSpeed)
    }
    this.node.x += this.xSpeed * dt

    if(this.node.x < this.minPosX) {
      this.node.x = this.minPosX;
      this.xSpeed = 0
    }
    if(this.node.x > this.maxPosX) {
      this.node.x = this.maxPosX;
      this.xSpeed = 0
    }
  },
})
