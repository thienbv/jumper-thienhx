cc.Class({
  extends: cc.Component,

  properties: {
    starPrefab: {
      default: null,
      type: cc.Prefab,
    },
    scoreFxPrefab: {
      default: null,
      type: cc.Prefab,
    },
    maxStarDuration: 0,
    minStarDuration: 0,

    ground: {
      default: null,
      type: cc.Node,
    },

    player: {
      default: null,
      type: cc.Node,
    },

    scoreDisplay: {
      default: null,
      type: cc.Label,
    },
    hightScore: {
      default: null,
      type: cc.Label,
    },
    btnPlay: {
      default: null,
      type: cc.Node,
    },
    scoreAudio: {
      default: null,
      type: cc.AudioClip,
    },
    starProgress: {
      default: null,
      type: cc.ProgressBar,
    },
  },

  onLoad: function () {
    this.score = 0
    this.timer = 0
    this.starDuration = 0
    this.currentScore = 0
    this.currentStar = null
    this.isGameRunning = true
    this.btbPlayY = this.btnPlay._position.y
    this.btnPlay.setPosition(3000, this.btbPlayY)
    this.starProgress.progress = 1
    this.spawNewStar()

    //set height score
    let heightScore = cc.sys.localStorage.getItem('#JUMPER-HEIGHTSCORE')
    heightScore = (heightScore) ? heightScore : 0;
    this.hightScore.string = `Height Score: ${heightScore}`
  },

  spawNewStar: function () {
    var newStar = cc.instantiate(this.starPrefab)
    this.node.addChild(newStar)
    newStar.setPosition(this.getNewStarPosition())
    newStar.getComponent('Star').game = this
    this.starDuration = this.minStarDuration + cc.random0To1() *
      (this.maxStarDuration - this.minStarDuration)
    this.timer = 0
    this.currentStar = newStar

  },

  getNewStarPosition: function () {
    var max = this.player.getComponent('Player').jumpHeight
    var min = this.ground._position.y

    var randY = Math.floor(cc.random0To1() * max + min)

    var maxX = this.node.width / 2
    var randX = (Math.random() - 0.5) * 2 * maxX

    return cc.p(randX, randY)
  },

  gainScore: function (pos) {
    this.score += 1
    this.currentScore = this.score
    let prefa = cc.instantiate(this.scoreFxPrefab)
    let fx = prefa.getComponent('ScoreFx')
    this.node.addChild(fx.node)
    fx.node.setPosition(pos)
    fx.play()

    this.scoreDisplay.string = `Score: ${this.score}`
    cc.audioEngine.playEffect(this.scoreAudio, false)
  },

  gameOver: function () {
    let hightScore = cc.sys.localStorage.getItem("#JUMPER-HEIGHTSCORE");
    if(hightScore < this.currentScore) {
      cc.sys.localStorage.setItem("#JUMPER-HEIGHTSCORE", this.currentScore);
    }
    this.player.active = false
    if (this.currentStar) {
      this.currentStar.destroy()
    }
    this.isGameRunning = false
    this.btnPlay.setPosition(0, this.btbPlayY)
    this.player.stopAllActions()
    // cc.director.loadScene('game')
  },

  update: function (dt) {
    if (!this.isGameRunning) {
      return
    }
    if (this.timer > this.starDuration) {
      this.gameOver()
      return
    }
    this.timer += dt
  },
})
